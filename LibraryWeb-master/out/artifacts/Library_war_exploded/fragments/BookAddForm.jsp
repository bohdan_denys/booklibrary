<%--
  Created by IntelliJ IDEA.
  User: Bogd_jc64
  Date: 13.10.2021
  Time: 13:05
  To change this template use File | Settings | File Templates.
--%>
<link rel="stylesheet" href="css/main.css" />
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="book-add-form">
    <form method="post" enctype="multipart/form-data">
        <input id="cover" name="cover" type="file">
        <label for="cover"><img src="" id="coverImg"/></label>
        <label>Author <input name="author"></label>
        <br/>
        <label>Title <input name="title"></label>
        <br/>
        <input type="submit" value="Add"/>
    </form>
</div>
<script src="js/bookaddform.js"></script>
