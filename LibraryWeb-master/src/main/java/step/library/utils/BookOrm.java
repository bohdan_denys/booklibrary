package step.library.utils;

import org.json.JSONObject;
import step.library.models.Book;

import java.sql.*;

public class BookOrm {

    private Connection connection;
    private final String PREFIX;
    private final JSONObject config;

    BookOrm (Connection connection, String PREFIX, JSONObject config) {
        this.connection = connection;
        this.PREFIX = PREFIX;
        this.config = config;
    }

    public boolean isTableExists() {
        String query;
        String dbms = config.getString( "dbms" ) ;
        if( dbms.equalsIgnoreCase( "Oracle") ) {
            query = "SELECT COUNT(*) " +
                    "FROM USER_TABLES T " +
                    "WHERE T.TABLE_NAME = '" + PREFIX + "BOOKS'";
        }
        else {
            return false;
        }
        try(ResultSet res = connection
                .createStatement()
                .executeQuery(query)) {
            if(res.next()) {
                return res.getInt(1) == 1;
            }
        } catch (SQLException ex) {
            System.err.println("BookOrm.isTableExists: " + ex.getMessage());

        }
        return false;
    }

    public boolean createTableBook() {
        String query;
        String dbms = config.getString("dbms");
        if( dbms.equalsIgnoreCase( "Oracle") ) {
            query = "CREATE TABLE " + PREFIX + "Books" +
                    "(Id          RAW(16) DEFAULT SYS_GUID() PRIMARY KEY, " +
                    " Title       NVARCHAR2(256) NOT NULL, " +
                    " Author      NVARCHAR2(400) NULL, " +
                    " Cover       NVARCHAR2(400) NULL )" ;
        }
        else {
            return false;
        }
        try(Statement statement = connection.createStatement()) {
            statement.executeQuery(query);
            return true;
        } catch (SQLException ex) {
            System.err.println("BookOrm.createTableBook: " + ex.getMessage() + " " + query);
        }
        return false;
    }

    public boolean add(Book book) {
        if(connection == null || book == null) return false;
        String query;
        String dbms = config.getString("dbms");
        if(dbms.equalsIgnoreCase("Oracle") || dbms.equalsIgnoreCase("MySQL")) {
            query = "INSERT INTO " + PREFIX + "Books" +
                    "(Title, Author, Cover) VALUES(?, ?, ?)";
        }
        else {
            return false;
        }
        try(PreparedStatement prep = connection.prepareStatement(query)) {
            prep.setString(1, book.getTitle());
            prep.setString(2, book.getAuthor());
            prep.setString(3, book.getCover());

            prep.executeUpdate();
            return true;
        } catch (SQLException ex) {
            System.err.println("BookOrm.add: " + ex.getMessage() + " " + query);
            return false;
        }
    }

    public Book[] getList() {
        if(connection == null) return null;
        String query, queryCount;
        String dbms = config.getString("dbms");
        if(dbms.equalsIgnoreCase("Oracle") || dbms.equalsIgnoreCase("MySQL")) {
            queryCount = "SELECT COUNT(*) FROM " + PREFIX + "BOOKS";
            query = "SELECT B.id, B.author, B.title, B.cover FROM "
                    + PREFIX + "BOOKS B";
        } else {
            return null;
        }
        try(Statement statement = connection.createStatement()) {
            ResultSet res = statement.executeQuery(queryCount);
            res.next();
            int cnt = res.getInt(1);
            res.close();
            res = statement.executeQuery(query);
            Book[] ret = new Book[cnt];
            for (int i = 0; i < cnt; i++) {
                res.next();
                ret[i] = new Book(
                        res.getString(1),
                        res.getString(2),
                        res.getString(3),
                        res.getString(4)
                );
            }
            return ret;
        } catch (SQLException ex) {
            System.err.println("BookOrm.getList: " + ex.getMessage() + "\n" + query);
        }
        return null;
    }
}
