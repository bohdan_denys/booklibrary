package step.library.utils;

import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;

public class Db {
    private static final String PREFIX = "KH181_3_" ;
    private static JSONObject config;
    private static Connection connection ;

    private static BookOrm bookOrm;

    public static BookOrm getBookOrm() {
        if(bookOrm == null)
            bookOrm = new BookOrm(connection, PREFIX, config);
        return bookOrm;
    }

    public static Connection getConnection() {
        return connection;
    }

    public static boolean setConnection( JSONObject json ) {
        try {
            String dbms = json.getString( "dbms" ) ;
            if( dbms.equalsIgnoreCase( "Oracle") ) {
                String connectionString;

                connectionString = String.format (
                        "jdbc:oracle:thin:%s/%s@%s:%s:XE",
                        json.get( "user" ),
                        json.get( "pass" ),
                        json.get( "host" ),
                        json.get( "port" )
                ) ;
                DriverManager.registerDriver(
                        new oracle.jdbc.driver.OracleDriver()
                );

                config = json;
                connection = DriverManager.getConnection(connectionString);
                return true ;
            } else {
                System.err.println( "Db: Unsupported DBMS" ) ;
            }
        } catch ( Exception ex ) {
            System.err.println( "Db: " + ex.getMessage() ) ;
        }
        config = null;
        connection = null ;
        return false ;
    }
}
